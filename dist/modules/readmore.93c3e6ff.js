'use strict';

angular.module('subexpressApp')
  .controller('ReadMoreIndexCtrl', function ($state,$stateParams,$rootScope,$scope,config,$window,$uibModal,$translate,
    FileUploader) {

    $(".preloader").addClass("loaded")

    $scope.goToSection = function(id_name){
      $('html,body').animate(
        {
          scrollTop: $("#"+id_name).offset().top
        },
        300
      );
    }
  });
