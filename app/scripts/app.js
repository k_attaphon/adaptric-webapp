'use strict';

/**
 * @ngdoc overview
 * @name subexpressApp
 * @description
 * # subexpressApp
 *
 * Main module of the application.
 */
 var app;
app = angular
  .module('subexpressApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    // "oc.lazyLoad",
    "ui.router",
    "ui.bootstrap",
    "restangular",
    "ui.utils.masks",
    "selector",
    "angularFileUpload",
    "highcharts-ng",
    // "rzSlider",
    "pascalprecht.translate",
    "ngMask",
    "chart.js"
  ]);

app.factory("settings", [
  "$rootScope",
  function($rootScope) {
    var settings;
    settings = {
      layout: {
        pageSidebarClosed: false,
        pageContentWhite: true,
        pageBodySolid: false,
        pageAutoScrollOnLoad: 1000
      },
      assetsPath: "../assets",
      globalPath: "../assets/global",
      layoutPath: "../assets/layouts/layout2"
    };
    $rootScope.settings = settings;
    return settings;
  }
]);


// app.config([
//   "$ocLazyLoadProvider",
//   function($ocLazyLoadProvider) {
//     $ocLazyLoadProvider.config({});
//   }
// ]);

app
  .constant("config", {
    // apiUrl: "https://api.subexpress.at44.co.th",
    apiUrl: "http://localhost:3001",
  })
  .config([
    "$httpProvider",
    "$stateProvider",
    "$urlRouterProvider",
    "RestangularProvider",
    "$compileProvider",
    "config",
    function(
      $httpProvider,
      $stateProvider,
      $urlRouterProvider,
      RestangularProvider,
      $compileProvider,
      config
    ) {
      // $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
      // $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|local|data|chrome-extension):/);
      // $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|local|data|chrome-extension):/);
      // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)

      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common["X-Requested-With"];
      // RestangularProvider.setRequestSuffix('.json');

     

      // RestangularProvider.setBaseUrl(config.apiUrl);
      // $urlRouterProvider.otherwise("/main/index");
      $urlRouterProvider.otherwise("/main/index");
      // $urlRouterProvider.otherwise("/main/index");
      return $stateProvider
        .state("main", {
          url: "/main",
          templateUrl: "modules/main.html",
          controller: "MainCtrl",
        })
        .state("main.index", {
          url: "/index",
          templateUrl: "modules/index.html",
          controller: "MainIndexCtrl"
        })
        .state("main.readmore", {
          url: "/readmore",
          templateUrl: "modules/readmore.html",
          controller: "ReadMoreIndexCtrl"
        })
    }
  ]);
app.run([
  "$rootScope",
  "$window",
  "Restangular",
  "$state",
  "settings",
  "$transitions",
  "config",
  "$translate",
  function(
    $rootScope,
    $window,
    Restangular,
    $state,
    settings,
    $transitions,
    config,
    $translate
  ) {
    Restangular.addRequestInterceptor(function(element) {
      $rootScope.req = true;
      return element;
    });
    Restangular.addResponseInterceptor(function(data) {
      $rootScope.req = false;
      return data;
    });

    $rootScope.$state = $state;
    $rootScope.$settings = settings;
    $rootScope.config = config;
    $rootScope.$translate = $translate;
    $transitions.onSuccess({}, function(trans) {
      // console.log("statechange success");
      // console.log(trans);
    });
    $transitions.onStart({}, function(trans) {
      // console.log("statechange start");
      for(var i = 1; i < 99999; i++){
        window.clearInterval(i);
      }
    });

    $transitions.onError({}, function(trans) {
      // console.log(trans);
      if (trans._targetState._definition.name != $state.current.name) {
        $window.location.href = "/?#!/main/index";
      }
    });

    // $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    //   console.log(123)
    // });
    // $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
    //   console.log('stateChangeError');
    //   event.preventDefault();
    //   $state.go('/login/index')
    // });
  }
]);